import { PromisoryNote } from "../types/PromisoryNote";

export const PromisoryNotes:PromisoryNote[] = [
    {
        name: "Support for the Throne",
        nickname: "Support"
    },
    {
        name: "Trade Agreement",
        nickname: "Trade"
    },
    {
        name: "Ceasefire",
        nickname: "Ceasefire"
    },
    {
        name: "Political Secret",
        nickname: "Secret"
    },
    {
        name: "Alliance",
        nickname: "Alliance"
    }
]