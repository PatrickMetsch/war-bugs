import { Color } from "../types/Color";

export const Colors:Color[] =
[
    {
        name: "Red",
        hex: "#FF0000"
    },
    {
        name: "Green",
        hex: "#00FF00"
    },
    {
        name: "Blue",
        hex: "#0000FF"
    },
    {
        name: "Black",
        hex: "#000000"
    },
    {
        name: "Yellow",
        hex: "#FFFF00"
    },
    {
        name: "Purple",
        hex: "#800080"
    }
]