import { TechnologyType } from "../types/TechnologyType";
import { Colors } from "./Colors";

const colorFinder = (colorName:string) => Colors.find(c => c.name === colorName)!

export const TechnologyTypes:TechnologyType[] = [
    {
        name: "Biotic",
        color: colorFinder("Green")
    },
    {
        name: "Propulsion",
        color: colorFinder("Blue")
    },
    {
        name: "Cybernetic",
        color: colorFinder("Yellow")
    },
    {
        name: "Warfare",
        color: colorFinder("Red")
    },
    {
        name: "Unit",
        color: colorFinder("Black")
    }
]