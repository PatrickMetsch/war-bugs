import { Faction } from "../types/Faction";
import { Technology, TechnologyReplacer } from "../types/Technology";
import { Technologies } from "./Technologies";
import { TechnologyTypes } from "./TechnologyTypes";

const factionUpgradeMapper = 
    (upgradeName:string) =>
    (replaces:string):TechnologyReplacer => 
        ({ 
            name: upgradeName, 
            type: TechnologyTypes
                .find(({name}) => name === "Unit")!,
            replaces: Technologies.find(t => t.name === replaces)!
        })

const factionTechMapper = 
    (typeName:string) =>
    (techName:string):Technology =>
        ({
            name: techName,
            type: TechnologyTypes.find(t => t.name === typeName)!
        })

const bioticTechCalled = factionTechMapper("Biotic")
const propulsionTechCalled = factionTechMapper("Propulsion")
const cyberneticTechCalled = factionTechMapper("Cybernetic")
const warfareTechCalled = factionTechMapper("Warfare")

export const Factions:Faction[] =
[
    {
        name: "The Arborec",
        nickname: "Arborec",
        factionPromisoryNote: {
            name: "Stymie",
            nickname: "Stymie"
        },
        factionUnitUpgrades: factionUpgradeMapper("Letani Warrior II")("Infantry II"),
        factionTechnology: [bioticTechCalled("Bioplasmosis")]
    },
    // {
    //     name: "The Argent Flight",
    //     nickname: "Argent",
    //     factionPromisoryNote: {
    //         name: "",
    //         nickname: ""
    //     }
    // },
    {
        name: "The Barony of Letnev",
        nickname: "Barony",
        factionPromisoryNote: {
            name: "War Funding",
            nickname: "War Funding"
        },
        factionTechnology: [cyberneticTechCalled("L4 Disruptors"), warfareTechCalled("Non-Euclidian Shielding")]
    },
    {
        name: "The Clan of Saar",
        nickname: "Saar",
        factionPromisoryNote: {
            name: "Ragh's Call",
            nickname: "Ragh"
        },
        factionUnitUpgrades: factionUpgradeMapper("Floating Factory II")("Space Dock II"),
        factionTechnology: [propulsionTechCalled("Chaos Mapping")]
    },
    {
        name: "The Embers of Muaat",
        nickname: "Muaat",
        factionPromisoryNote: {
            name: "Fires of the Gashlai",
            nickname: "Gashlai"
        },
        factionUnitUpgrades: factionUpgradeMapper("Prototype War Sun II")("War Sun"),
        factionTechnology: [warfareTechCalled("Magmus Reactor")]
    },
    {
        name: "The Emirates of Hacan",
        nickname: "Hacan",
        factionPromisoryNote: {
            name: "Trade Convoys",
            nickname: "Convoys"
        },
        factionTechnology: [bioticTechCalled("Production Biomes"), cyberneticTechCalled("Quantum Datahub Node")]
    },
    // {
    //     name: "The Empyrean",
    //     nickname: "Empyrean",
    //     factionPromisoryNote: {
    //         name: "",
    //         nickname: ""
    //     }
    // },
    {
        name: "The Federation of Sol",
        nickname: "Sol",
        factionPromisoryNote: {
            name: "Military Support",
            nickname: "Military"
        },
        factionUnitUpgrades: factionUpgradeMapper("Spec Ops II")("Infantry II")
    },
    {
        name: "The Ghosts of Creuss",
        nickname: "Creuss",
        factionPromisoryNote: {
            name: "Creuss Iff",
            nickname: "Iff"
        },
        factionTechnology: [propulsionTechCalled("Wormhole Generator"), warfareTechCalled("Dimenstional Splicer")]
    },
    {
        name: "The L1Z1X Mindnet",
        nickname: "L1Z1X",
        factionPromisoryNote: {
            name: "Cybernetic Enhancements",
            nickname: "Cybernetic"
        },
        factionUnitUpgrades: factionUpgradeMapper("Super Dreadnought II")("Dreadnought II"),
        factionTechnology: [cyberneticTechCalled("Inheritance Systems")]
    },
    // {
    //     name: "The Mahact Gene Sorcerers",
    //     nickname: "Mahact",
    //     factionPromisoryNote: {
    //         name: "",
    //         nickname: ""
    //     }
    // },
    {
        name: "The Mentak Coalition",
        nickname: "Mentak",
        factionPromisoryNote: {
            name: "Promise of Protection",
            nickname: "Protection"
        },
        factionTechnology: [cyberneticTechCalled("Salvage Operations"), cyberneticTechCalled("Mirror Computing")]
    },
    {
        name: "The Naalu Collective",
        nickname: "Naalu",
        factionPromisoryNote: {
            name: "Gift of Prescience",
            nickname: "Gift"
        },
        factionUnitUpgrades: factionUpgradeMapper("Hybrid Crystal Fighter II")("Fighter II"),
        factionTechnology: [bioticTechCalled("Neuroglaive")]
    },
    // {
    //     name: "The Naaz-Rokha Alliance",
    //     nickname: "Naaz",
    //     factionPromisoryNote: {
    //         name: "",
    //         nickname: ""
    //     }
    // },
    {
        name: "The Nekro Virus",
        nickname: "Nekro",
        factionPromisoryNote: {
            name: "Antivirus",
            nickname: "Antivirus"
        },
    },
    {
        name: "The Sardakk N'orr",
        nickname: "Sardaak",
        factionPromisoryNote: {
            name: "Tekklar Legion",
            nickname: "Tekklar"
        },
        factionUnitUpgrades: factionUpgradeMapper("Exotrireme II")("Dreadnought II"),
        factionTechnology: [warfareTechCalled("Valkyrie Particle Weave")]
    },
    // {
    //     name: "The Titans of Ul",
    //     nickname: "Titans",
    //     factionPromisoryNote: {
    //         name: "",
    //         nickname: ""
    //     }
    // },
    {
        name: "The Universities of Jol-Nar",
        nickname: "Jol-Nar",
        factionPromisoryNote: {
            name: "Research Agreement",
            nickname: "Research"
        },
        factionTechnology: [cyberneticTechCalled("E-Res Siphons"), propulsionTechCalled("Spacial Conduit Cylinder")]
    },
    // {
    //     name: "The Vuil'Raith Cabal",
    //     nickname: "Cabal",
    //     factionPromisoryNote: {
    //         name: "",
    //         nickname: ""
    //     }
    // },
    {
        name: "The Winnu",
        nickname: "Winnu",
        factionPromisoryNote: {
            name: "Acquiescence",
            nickname: "Acquiescence"
        },
        factionTechnology: [cyberneticTechCalled("Hegemonic Trade Policy"), propulsionTechCalled("Lazax Gate Folding")]
    },
    {
        name: "The Xxcha Kingdom",
        nickname: "Xxcha",
        factionPromisoryNote: {
            name: "Political Favor",
            nickname: "Favor"
        },
        factionTechnology: [cyberneticTechCalled("Nullification Field"), bioticTechCalled("Instinct Training")]
    },
    {
        name: "The Yin Brotherhood",
        nickname: "Yin",
        factionPromisoryNote: {
            name: "Greyfire Mutagen",
            nickname: "Greyfire"
        },
        factionTechnology: [cyberneticTechCalled("Impulse Core"), bioticTechCalled("Yin Spinner")]
    },
    {
        name: "The Yssaril Tribes",
        nickname: "Yssaril",
        factionPromisoryNote: {
            name: "Spy Net",
            nickname: "Spy Net"
        },
        factionTechnology: [bioticTechCalled("Transparasteel Plating"), bioticTechCalled("Mageon Implants")]
    },
]