import { Technology } from "../types/Technology";
import { TechnologyTypes } from "./TechnologyTypes";

const techFinder = (techTypeName:string) => TechnologyTypes.find(t => t.name === techTypeName)!

const techMapper = 
    (techNames:string[]) =>
    (techTypeName:string):Technology[] => 
    techNames.map(t => 
        ({
            name: t,
            type: techFinder(techTypeName)
        })
    )

const bioticTechnologies:Technology[] = 
    techMapper
        ([
            "Neural Motivator",
            "Dacxive Animators",
            "Hyper Metabolism",
            "X-89 Bacterial Weapon",
        ])
        ("Biotic")

const propulsionTechnologies:Technology[] =
    techMapper
        ([
            "Antimass Deflectors",
            "Gravity Drive",
            "Fleet Logistics",
            "Light/Wave Deflector"
        ])
        ("Propulsion")

const cyberneticTechnologies:Technology[] =
    techMapper
        ([
            "Sarween Tools",
            "Graviton Laser System",
            "Transit Diodes",
            "Integrated Economy"
        ])
        ("Cybernetic")

const warfareTechnologies:Technology[] =
    techMapper
        ([
            "Plasma Scoring",
            "Magen Defense Grid",
            "Duranium Armor",
            "Assault Cannon"
        ])
        ("Warfare")

const unitTechnologies:Technology[] =
    techMapper
        ([
            "Infantry II",
            "Carrier II",
            "Space Dock II",
            "Destroyer II",
            "Fighter II",
            "PDS II",
            "Dreadnought II",
            "Cruiser II",
            "War Sun"
        ])
        ("Unit")

export const Technologies:Technology[] = [
    ...bioticTechnologies,
    ...propulsionTechnologies,
    ...cyberneticTechnologies,
    ...warfareTechnologies,
    ...unitTechnologies
]