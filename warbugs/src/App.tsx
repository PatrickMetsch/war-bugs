import React from 'react';
import { PlayerSelect } from './components/PlayerSelect';
import { SiteProps } from './types/SiteProps';
import { jsonString } from './util/json/jsonString';

export const App = 
	({availableFactions, availableColors, isPlayersSet, players, newPlayerToAdd, updateNewPlayerToAdd, addPlayer, updatePlayersSet}:SiteProps) => 
	(
		<div>
			{!isPlayersSet && (
				PlayerSelect
					(newPlayerToAdd)
					(updateNewPlayerToAdd)
					(availableFactions)
					(availableColors)
					(addPlayer)
					(updatePlayersSet)
			)}

			<h1>Current Player</h1>
			<pre>{jsonString(newPlayerToAdd)}</pre>
			<h1>All Players</h1>
			<pre>{jsonString(players)}</pre>
		</div>
	)