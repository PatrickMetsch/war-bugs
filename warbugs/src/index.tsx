import { App } from "./App"
import { reactRender } from "./util/react/reactRender"
import { SiteProps } from "./types/SiteProps"
import { Factions } from "./models/Factions"
import { Faction } from "./types/Faction"
import { Colors } from "./models/Colors"
import { Color } from "./types/Color"
import { PlayerIdentity } from "./types/Player"
import { PromisoryNotes } from "./models/PromisoryNotes"
import { Technologies } from "./models/Technologies"
import { Technology } from "./types/Technology"
import { el } from "./util/dom/el"

export const replaceTechWithFactionTech = (faction:Faction):Technology[] => {
	const defaultTech = Technologies
	const factionUnitUpgrades = faction.factionUnitUpgrades!

	return [
		...defaultTech.filter(t => t.name !== factionUnitUpgrades.replaces.name), 
		({
			name: factionUnitUpgrades.name, 
			type: factionUnitUpgrades.type
		})
	]
}

export const renderApp = (state:any) => {

	const updateNewPlayerToAdd = (updatedPlayerInfo: Partial<PlayerIdentity>) =>
		renderApp({
			...state,
			newPlayerToAdd: {
				...state.newPlayerToAdd,
				...updatedPlayerInfo,
			},
		})

	const addPlayer = () => {
		const { faction } = state.newPlayerToAdd

		renderApp({
			...state,
			players: [
				...state.players, 
				{
					...state.newPlayerToAdd,
					promisoryNotes: [
						...PromisoryNotes,
						faction.factionPromisoryNote
					],
					availableTechnology: [
						...faction.factionUnitUpgrades ? replaceTechWithFactionTech(faction) : Technologies,
						...faction.factionTechnology ?? []],
					ownedTechnology: []
				}
			],
			availableColors: state.availableColors!
				.filter(
					(c: Color) => c.name !== state.newPlayerToAdd.color.name
				),
			availableFactions: state.availableFactions!
				.filter(
					(f: Faction) => f.name !== state.newPlayerToAdd.faction.name
				),
			newPlayerToAdd: {},
			isPlayersSet: state.players.length === 5
		})
	}

	const updatePlayersSet = (isPlayersSet:boolean) => {
		renderApp({
			...state,
			isPlayersSet
		})
	}

	const siteProps: SiteProps = {
		...state,
		addPlayer,
		updateNewPlayerToAdd,
		updatePlayersSet,
	}

	reactRender
		(App(siteProps))
		(el("root"))
}

const initialState: Partial<SiteProps> = {
	availableFactions: Factions,
	players: [],
	isPlayersSet: false,
	availableColors: Colors,
	newPlayerToAdd: {},
}

renderApp(initialState)
