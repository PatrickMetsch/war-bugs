import React from "react"
import { Colors } from "../models/Colors"
import { Factions } from "../models/Factions"
import { Color } from "../types/Color"
import { Faction } from "../types/Faction"
import { PlayerIdentity } from "../types/Player"
import { StateEvent, StateUpdater } from "../types/StateUpdater"
import { FactionSelect } from "./itemSelect/FactionSelect"
import { ColorSelect } from "./itemSelect/ColorSelect"
import { fragment } from "../util/react/fragment"
import { isWholePlayer } from "../util/model/isWholePlayer"
import { doNothing } from "../util/axiom/doNothing"

export const PlayerSelect = 
	(newPlayerToAdd:Partial<PlayerIdentity>) => 
	(updateNewPlayerToAdd: StateUpdater<Partial<PlayerIdentity>>) =>
	(availableFactions:Faction[]) =>
	(availableColors:Color[]) =>
	(addPlayer:StateEvent) => 
	(updatePlayersSet:StateUpdater<boolean>) =>
		fragment([
			<div id="playerSetupContainer">
				<input type="text" placeholder="Player Name" id="playerNameInput" value={newPlayerToAdd.name ?? ""} onChange={(e) => updateNewPlayerToAdd({name: e.target.value})} />,
				{
					FactionSelect
						(newPlayerToAdd.faction?.nickname)
						(availableFactions)
						(({target: {value}}) => updateNewPlayerToAdd({faction: Factions.find(({nickname}) => nickname === value)}))
				}
				{
					ColorSelect
						(newPlayerToAdd.color?.name)
						(availableColors)
						(({target: {value}}) => updateNewPlayerToAdd({color: Colors.find(({name}) => name === value)}))
				}
			</div>,
			<button onClick={() => isWholePlayer(newPlayerToAdd) ? addPlayer() : doNothing()}>Add Player</button>,
			<button onClick={() => updatePlayersSet(true)}>Done</button>
		])