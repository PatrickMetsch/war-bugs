import React, { ChangeEvent } from "react";
import { StateUpdater } from "../../types/StateUpdater";

export const ItemSelect =
	(id:string) =>
	(defaultOptionTitle:string) =>
	(value:string | undefined) =>
	<T extends {name:string, nickname?:string}[]>(availableOptions:T) =>
	(callback:StateUpdater<ChangeEvent<HTMLSelectElement>>) =>
	(
		<select 
			id={id} 
			value={value ?? ""}
			onChange={(e) => callback(e)}
		>
			<option value="default">{defaultOptionTitle}</option>
			{
				availableOptions.map(o =>
					<option value={o.nickname ?? o.name}>{o.name}</option>	
				)
			}

		</select>
	)
