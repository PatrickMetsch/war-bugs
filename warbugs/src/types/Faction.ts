import { PromisoryNote } from "./PromisoryNote";
import { Technology, TechnologyReplacer } from "./Technology";

export type Faction = {
    name:string,
    nickname:string,
    factionPromisoryNote:PromisoryNote
    factionUnitUpgrades?:TechnologyReplacer,
    factionTechnology?:Technology[]
}
