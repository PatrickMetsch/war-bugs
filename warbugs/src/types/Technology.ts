import { Color } from "./Color";
import { TechnologyType } from "./TechnologyType";

export type Technology = {
    name:string,
    type:TechnologyType
}

export type TechnologyReplacer = Technology & {replaces:Technology}
