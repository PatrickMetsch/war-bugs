export type StateUpdater<T> = (t:T) => void
export type StateEvent = () => void
