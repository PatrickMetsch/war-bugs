import { Color } from "./Color"

export type TechnologyType = {
    name:string,
    color:Color
}