import { Color } from "./Color";
import { Faction } from "./Faction";
import { Player } from "./Player";
import { StateEvent, StateUpdater } from "./StateUpdater";

export type SiteProps = {
	availableFactions:Faction[],
	removeAvailableFactionByName:StateUpdater<string>,
	players:Player[],
	isPlayersSet:boolean,
	availableColors:Color[],
	removeAvailableColorByName:StateUpdater<string>,
	newPlayerToAdd: Partial<Player>,
	updateNewPlayerToAdd:StateUpdater<Partial<Player>>,
	addPlayer:StateEvent,
	updatePlayersSet:StateUpdater<boolean>,
}
