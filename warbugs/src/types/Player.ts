import { Color } from "./Color"
import { Faction } from "./Faction"
import { PromisoryNote } from "./PromisoryNote"
import { Technology } from "./Technology"

export type Player = PlayerIdentity & {
    score:number,
    promisoryNotes:PromisoryNote[],
    availableTechnology: Technology[],
    ownedTechnology: Technology[]
}

export type PlayerIdentity = {
	name:string,
	color:Color,
	faction:Faction
}
