import { PlayerIdentity } from "../../types/Player";

export const isWholePlayer = 
	(possibleWholePlayer:PlayerIdentity | Partial<PlayerIdentity>):possibleWholePlayer is PlayerIdentity =>
		Object.values(possibleWholePlayer).length === 3
