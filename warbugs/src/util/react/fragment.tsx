import React, { Fragment } from "react"

export const fragment = 
	(content:JSX.Element[]) =>
		<Fragment>
			{content}
		</Fragment>