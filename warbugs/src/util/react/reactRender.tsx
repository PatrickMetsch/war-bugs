import ReactDOM from 'react-dom';

export const reactRender = 
  (what:JSX.Element) =>
  (where:HTMLElement | null) => {
    ReactDOM.render(what, where)
  }
