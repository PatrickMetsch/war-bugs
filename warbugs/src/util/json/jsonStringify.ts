export const jsonStringify =
	(json:any) =>
	(replacer:(string | number)[] | null | undefined) =>
	(space: string | number | undefined) =>
		JSON.stringify(json, replacer, space)